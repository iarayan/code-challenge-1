#!/usr/bin/env node

const add = require('./operations/add');
const list = require('./operations/list');
const get = require('./operations/get');
const remove = require('./operations/remove');
const clear = require('./operations/clear');

const [, , ...args] = process.argv;


const store = (parameters) => {
    if (typeof(parameters) === 'undefined' || !parameters.length) {
        console.log(`Kindly use one of the following commands\nadd key value\nlist\nget key\nremove key\nclear`);
        return;
    }

    switch (parameters[0]) {
        case 'add':
            add(parameters[1], parameters[2]);
            break;
        case 'list':
            list();
            break;
        case 'get':
            get(parameters[1]);
            break;
        case 'remove':
            remove(parameters[1]);
            break;
        case 'clear':
            clear();
            break;
        default:
            console.log('Kindly use one of the following commands\nadd key value\nlist\nget key\nremove key\nclear');
            break;
    }
};

store(args);
