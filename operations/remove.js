const fs = require('fs');
const rebuild = require('./additions/rebuild');

module.exports = (key) => {

    // Get our current store
    let storeReference = {
        store: {}
    };

    fs.readFile('data.json', null, (err, data) => {
        if (typeof(key) === 'undefined') {
            console.log('To remove a key from the store, the key must be provided e.g.: \'node store.js remove <key>\' replacing <key> with the desired input.');
            return;
        }

        if (err) {

            // Rebuild the store
            rebuild('The local store is corrupted, rebuilding...', storeReference);
        }

        try {

            storeReference.store = JSON.parse(data);
        } catch (e) {

            // Rebuild the store
            rebuild('The local store contains unidentified data, rebuilding...', storeReference);
        }

        if (storeReference.store.hasOwnProperty(key)) {
            delete storeReference.store[key];
            console.log(`The key ${key} was removed from the store.`);
            fs.writeFile('data.json', JSON.stringify(storeReference.store), null, (err) => {
                if (err) throw err;
            });
            return;
        }

        console.log(`The key ${key} doesn't exist in the store.`);
    });
};
