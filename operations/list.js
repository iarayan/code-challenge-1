const fs = require('fs');
const rebuild = require('./additions/rebuild');
module.exports = () => {

    // Get our current store
    let storeReference = {
        store: {}
    };
    fs.readFile('data.json', null, (err, data) => {
        if (err) {
            // Rebuild the store
            rebuild('The local store is corrupted, rebuilding...', storeReference);
        }

        try {
            storeReference.store = JSON.parse(data);
        } catch (e) {
            // Rebuild the store
            rebuild('The local store contains unidentified data, rebuilding...', storeReference);
        }

        for (let key in storeReference.store) {
            console.log(`${key} => ${storeReference.store[key]}`);
        }

        console.log(`Total: ${Object.keys(storeReference.store).length} key(s).`);
    });
};
