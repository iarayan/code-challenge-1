const rebuild = require('./additions/rebuild');

module.exports = () => {

    // Get our current store
    let storeReference = {
        store: {}
    };
    rebuild('Clearing the store...', storeReference, () => {
        console.log('Completed.');
    });

};
