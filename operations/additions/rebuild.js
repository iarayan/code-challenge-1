const fs = require('fs');

module.exports = (message, storeReference, callback) => {

    // Notify the user that the store is corrupted
    console.log(message);

    // Reset the store
    storeReference.store = {};

    // Save it to the file system
    fs.writeFile('data.json', JSON.stringify(storeReference.store), null, (err) => {
        if (err) throw err;
        typeof callback === 'function' && callback();
    });
};