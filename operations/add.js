const fs = require('fs');
const rebuild = require('./additions/rebuild');

module.exports = (key, value) => {
    if (typeof(key) === 'undefined' || typeof(value) === 'undefined') {
        console.log('To add a key value pair, use the following command: \'node store.js add <key> <value>\' replacing both <key> and <value> with the desired input.');
        return;
    }

    // Get our current store
    let storeReference = {
        store: {}
    };
    fs.readFile('data.json', null, (err, data) => {

        // The message shown to the user
        let operation = 'Added';

        if (err) {
            // Rebuild the store
            rebuild('The local store is corrupted, rebuilding...', storeReference);
        }
        try {
            storeReference.store = JSON.parse(data);
        } catch (e) {
            // Rebuild the store
            rebuild('The local store contains unidentified data, rebuilding...', storeReference);
        }


        if (storeReference.store.hasOwnProperty(key)) {
            console.log(`The key ${key} already exists, updating it's value...`)
            operation = 'Updated';
        }

        storeReference.store[key] = value;

        fs.writeFile('data.json', JSON.stringify(storeReference.store), null, (err) => {
            if (err) throw err;
            console.log(`${operation} ${key} => ${value}`);
        });
    });
};
