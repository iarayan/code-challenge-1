const fs = require('fs');
const rebuild = require('./additions/rebuild');

module.exports = (key) => {

    // Get our current store
    let storeReference = {
        store: {}
    };

    fs.readFile('data.json', null, (err, data) => {
        if (typeof(key) === 'undefined') {
            console.log('To get the value of a key in the store, the key must be provided e.g.: \'node store.js get <key>\' replacing <key> with the desired input.');
            return;
        }

        if (err) {

            // Rebuild the store
            rebuild('The local store is corrupted, rebuilding...', storeReference);
        }

        try {

            storeReference.store = JSON.parse(data);
        } catch (e) {

            // Rebuild the store
            rebuild('The local store contains unidentified data, rebuilding...', storeReference);
        }

        const value = storeReference.store[key];

        if (typeof value === 'undefined') {
            console.log(`The key ${key} doesn't exist in the store.`);
            return;
        }

        console.log(`${key} => ${value}`);
    });
};
